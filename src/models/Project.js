const mongoose = require('mongoose');

const Project = mongoose.model('projects',{
    name:{
        type:String,
        required:true
    },
    text:{
        type:String,
        required:true
    },
    url:{
        type:String,
        required:true
    },
    site:{
        type:String,
        required:true
    },
    repo:{
        type:String,
        required:true
    }
})

module.exports = Project;