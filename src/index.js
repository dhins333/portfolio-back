const express = require('express');
const path = require('path');

require('./db/db');
const admin = require('./routes/admin');
const api = require('./routes/api');

const app = express();

app.use(express.static(path.join(__dirname,'..','public')));
app.use(express.json());
app.use(admin);
app.use(api);

app.get('*',(req,res) => {
    res.sendFile(path.join(__dirname,'..','public','index.html'));
})

app.listen(process.env.PORT,() => {
    console.log('Server Started');
});