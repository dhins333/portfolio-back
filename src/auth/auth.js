const auth = (req,res,next) => {
    if(req.body.pass === process.env.PASS){
        next();
    }
    else{
        res.status(401).send('Invalid Password');
    }
}

module.exports = auth;