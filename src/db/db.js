const mongoose = require('mongoose');

const db = mongoose.connect(process.env.DB,{
    useCreateIndex:true,
    useNewUrlParser:true,
    useUnifiedTopology:true
},(e) => {
    if(!e){
        console.log('DB connected')
    }
    else{
        console.log(e);
    }
})

module.exports = db;