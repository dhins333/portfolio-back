const mongoose = require('mongoose');
const express = require('express');
const path = require('path');
const fs = require('fs');

const Project = require('../models/Project');

const router = new express.Router();

router.get('/api/projects',async (req,res) => {
    try{
        const data = await Project.find({}).select({_id:0,__v:0});
        res.status(200).send(JSON.stringify(data));
    }
    catch(e){
        console.log(e);
        res.status(500).send('Internal Server Error');
    }
})

router.get('/api/resume',(req,res) => {
    fs.readFile(
        path.join(__dirname,'..','..','public','resume.pdf'),(err,data) => {
            if(err){
                res.status(500).send('Error reading file')            
            }
            else{
                res.contentType("application/pdf");
                res.status(200).send(data);
            }
        }
    )
})

module.exports = router;