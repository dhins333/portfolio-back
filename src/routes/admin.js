const express = require('express');
const auth = require('../auth/auth');
const Project = require('../models/Project');

const router = new express.Router();

router.post('/admin/project',auth,async (req,res) => {
    try{
        const project = new Project({
            name:req.body.name,
            text:req.body.text,
            url:req.body.url,
            site:req.body.site,
            repo:req.body.repo
        });
        await project.save();
        res.status(200).send('Project Saved');
    }
    catch(e){
        console.log(e);
        res.status(500).send('Internal Server Error');
    }
})

module.exports = router;